import { Fiche } from './Fiche';

export class Refugee {
  constructor() {}
  public id?: number;
  public firstname?: string;
  public lastName?: string;
  public sex?: string;
  public dateOfBirth?: Date;
  public nationality?: string;
  public frenchlanguageLevel?: any;
  public englishlanguageLevel?: any;
  public highestDegree?: any;
  public yearsOfExperience?: number;
  public email?: string;
  public fieldOfWork?: any;
  public adress?: any;
  public phoneNumber?: number;
  public fiche?: Fiche;
}
