export class Fiche {
    public id: number;
    public apparences?: string;
    public bloodpressure: number;
    public bloodtype: string;
    public description: string;
    public doctorname: string;
    public height: number;
    public mentalstate: string;
    public weight: number;
  }