import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Refugee } from './../entities/Refugee';
import { Injectable } from '@angular/core';
@Injectable()
export class RefugeeService {
  // tslint:disable-next-line:max-line-length
  token = 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJpZCI6Mywicm9sZSI6IkNhbXBDaGVmIn0.W8_n4zNk3-7wGttYWkSreT6wuLtkemuKJ3-Pq9_vZJhSjQfa2NLaPCtGFZRk0LbrBKaar3k4ApTS_jdwkUcH5Q';
  url= 'http://localhost:18080/refugeesCamp-web/api/Refugees';
  header: HttpHeaders;
  httpOptions = {
    headers: new HttpHeaders({ 'Authorization': this.token , 'Content-Type': 'application/json'})
  };
  constructor(private http: HttpClient) {
    this.header = new HttpHeaders();
    this.header.set('Authorization', this.token);
    this.header.set('Content-Type', 'application/json');

  }

  public GetAllRefugees() {
    return this.http.get(this.url);
  }

  public AddRefugee(r: Refugee) {
      return this.http.post(this.url, JSON.stringify(r), this.httpOptions);
  }

  public updateRefugee(r: Refugee) {
    return this.http.put(this.url, JSON.stringify(r));
  }

  public delete(r: Refugee) {
    return this.http.delete(this.url + '/' + r.id, this.httpOptions);
  }

}
