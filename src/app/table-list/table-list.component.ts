import { Component, OnInit } from '@angular/core';
import { Refugee } from 'app/entities/Refugee';
import { RefugeeService } from 'app/services/refugee.service';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
  refugees: any= [];
  // tslint:disable-next-line:max-line-length
  refugee: Refugee = { 'firstname': null , 'lastName': null , 'sex': null, 'dateOfBirth': null , 'nationality': null, 'frenchlanguageLevel': null , 'englishlanguageLevel': null, 'highestDegree': null , 'yearsOfExperience': 3 , 'email': null, 'fieldOfWork': null, 'adress' : null, 'phoneNumber': null, 'fiche' : null  };
  constructor(private refugeeService: RefugeeService) { }

  ngOnInit() {
    // tslint:disable-next-line:max-line-length
    this.refugees = this.refugeeService.GetAllRefugees().subscribe((resp: Response) => {console.log(resp); this.refugees = resp; } );
  }

  AddRefugee() {
    this.refugeeService.AddRefugee(this.refugee).subscribe(
      res => null , () =>  {     this.refugees.push(Object.assign({}, this.refugee));
        this.refugee = {'id': null , 'firstname': null , 'lastName': null , 'sex': null, 'dateOfBirth': null ,
          'nationality': null, 'frenchlanguageLevel': null , 'englishlanguageLevel': null, 'highestDegree': null ,
           'yearsOfExperience': 3 , 'email': null, 'fieldOfWork': null, 'adress' : null, 'phoneNumber': null, 'fiche' : null  };
      }
          );
    // tslint:disable-next-line:max-line-length
      console.log('add with success !! ');
  }

  deleteRefugee(r: Refugee) {
    this.refugeeService.delete(r).subscribe();
  }

}
